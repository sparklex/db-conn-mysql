export default {
	preset: 'ts-jest/presets/js-with-ts-esm',
	testEnvironment: 'node',
	moduleNameMapper: {
		'^(\\.{1,2}/.*)\\.js$': '$1',
	},
	
	testTimeout: 18000,
	roots: [
		"src"
	],
	collectCoverage:true,
	collectCoverageFrom: ["**/*.ts"],
	coverageDirectory: "coverage",
	coverageReporters: ["lcov"],
	coverageThreshold: {
		global: {
			statements: 100,
			branches: 100,
			functions: 100,
			lines: 100
		}
	},
	clearMocks: true
};