export interface MySqlConnectionConfig {
	port?: number;
	host?: string;
	user?: string;
	database?: string;
	password?: string;
	supportBigNumbers?: boolean /*always true*/
}
