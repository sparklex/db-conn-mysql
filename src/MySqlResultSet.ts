import { ResultSet, ResultSetColumnMetadata, ResultSetMetaData } from "db-conn";
import { Connection, Query } from "mysql2";
import { toAnsiTypeColumns, toAnsiValueRows } from './Utils.js'

export class MySqlResultSet implements ResultSet {
	query: Query;
	conn: Connection;
	data: any;
	promise: Promise<boolean>;
	resolve: (value: boolean) => void;
	reject: (reason?: any) => void;
	firstTime: boolean;

	promiseMetadata: Promise<ResultSetMetaData>;
	resolveMetadata: (value: ResultSetMetaData) => void;
	rejectMetadata: (reason?: any) => void;
	metadata: ResultSetMetaData;
	finished: boolean;

	constructor(conn: Connection, query: Query) {
		this.conn = conn;
		this.query = query;
		this.firstTime = true;
		this.resetPromise();
		this.promiseMetadata = new Promise((resolve, reject) => {
			this.resolveMetadata = resolve;
			this.rejectMetadata = reject;
		});
		const that = this;
		this.finished = false;
		this.query.on('error', function (err) {
			console.error(err);
			that.reject(err);
		})
			.on('fields', function (fields) {
				that.metadata = toAnsiTypeColumns(fields);
				that.resolveMetadata(that.metadata);
			})
			.on('result', function (row) {
				that.conn.pause();
				that.data = toAnsiValueRows([row], that.metadata)[0];
				that.resolve(true);
			})
			.on('end', function () {
				that.resolve(false);
				that.finished = true;
			});

	};

	public async next(): Promise<boolean> {
		if (this.finished) {
			return false;
		}
		if (this.firstTime) {
			this.firstTime = false;
		} else {
			this.resetPromise();
			this.conn.resume();
		}
		return this.promise;
	};
	public getRow(): any {
		const data = this.data;
		this.data = null;
		return data;
	};
	public async close(): Promise<void> {
		while(await this.next()) {

		}
		//this.conn.end();
		/*const rt = new Promise<void>((resolve, reject) => {
			this.conn.end((error) => {
				if (error) {
					reject(error);
					return;
				}	
				resolve();
			});	
		});
		return rt;*/
	};
	resetPromise() {
		this.promise = new Promise((resolve, reject) => {
			this.resolve = resolve;
			this.reject = reject;
		});
	};
	async getMetadata(): Promise<ResultSetMetaData> {
		return this.promiseMetadata;
	};
}