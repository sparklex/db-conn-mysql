import { Connection, Dialect } from "db-conn";
import { Table } from "db-conn/dist/schema/Table.js";

export class MySqlDialect implements Dialect {
	public quote(): string {
		return "`";
	}
	public async table(conn: Connection, table: string): Promise<Table> {
		const sql = `SELECT TABLE_NAME as TableName, 
		COLUMN_NAME as ColumnName, 
		COLUMN_TYPE as ColumnType,
		ORDINAL_POSITION as OrdinalPosition, 
		COLUMN_DEFAULT as ColumnDefault,
		IS_NULLABLE as IsNullable, 
		CHARACTER_MAXIMUM_LENGTH as CharacterMaxiumLength, 
		NUMERIC_PRECISION as NumericPercision, 
		NUMERIC_SCALE as NumericScale, 
		COLUMN_COMMENT as ColumnComment 
		from information_schema.columns
	where TABLE_SCHEMA = DATABASE() and TABLE_NAME = ?
	order by TABLE_NAME, ORDINAL_POSITION`;
		const oTable: Table = {};
		oTable.name = table;
		oTable.fields = {};
		oTable.fieldsOrder = [];
		const list = await conn.executeQuery(sql, [table]);
		for (const row of list ) {
			const fieldName = row['ColumnName'];
			oTable.fieldsOrder.push(fieldName);
			oTable.fields[fieldName] = {};
		}
		return oTable;
	}

}