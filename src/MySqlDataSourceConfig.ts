import { MySqlConnectionConfig } from "./MySqlConnectionConfig.js";

export interface MySqlDataSourceConfig extends MySqlConnectionConfig{
    waitForConnections?: boolean;
    connectionLimit?: number;
    maxIdle?: number;
    idleTimeout?: number;
    queueLimit?: number;
    enableKeepAlive?: boolean;
    keepAliveInitialDelay?: number;
    supportBigNumbers?: boolean;
}
