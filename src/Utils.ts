import { ResultSetColumnMetadata, ResultSetMetaData, SQLType } from "db-conn";

export function toAnsiTypeColumns(metadata: any): ResultSetMetaData {
	const rt = [];
	for (let mField of metadata) {
		const field: ResultSetColumnMetadata = {};
		field.name = mField.name;
		field.length = mField.columnLength;
		field.type = toAnsiType(mField.columnType);
		rt.push(field);
	}
	return rt;
}

export function toAnsiType(dataTypeID: number): SQLType {
	switch (dataTypeID) {
		case 3:
			return SQLType.i32;
		case 8:
			return SQLType.i64;
		case 253:
			return SQLType.varchar;
		case 246:
			return SQLType.decimal;
		case 5:
			return SQLType.f64;
		case 7:
			return SQLType.datetime;
		case 10:
			return SQLType.date;
		case 11:
			return SQLType.time;
		case 245:
			return SQLType.json;
		/*case 25:
			return SqlType.clob;
		case 17:
			return SqlType.blob;	*/
	}
	return "unknown" as SQLType;
}


export function toAnsiValueRows(rows: any[], metadata: ResultSetMetaData): any[] {
	const rt = [];
	for (let row of rows) {
		const newRow = {} as any;
		for (let field of metadata) {
			const value = row[field.name];
			newRow[field.name] = toAnsiValue(value, field.type);

		}
		rt.push(newRow);
	}
	return rt;
}

export function toAnsiValue(value: any, type: SQLType): number | string {
	if (value === undefined || value === null) {
		return value;
	}
	let rt = null;
	let offset = null;
	switch (type) {
		case SQLType.i8:
		case SQLType.i16:
		case SQLType.i32:
		case SQLType.u8:
		case SQLType.u16:
		case SQLType.u32:
		case SQLType.varchar:
		case SQLType.nvarchar:
		case SQLType.text:
		case SQLType.f32:
		case SQLType.f64:
			return value;
		case SQLType.i64:
			return value.toString();
		case SQLType.datetime:
			offset = value.getTimezoneOffset();
			value = new Date(value.getTime() - (offset * 60 * 1000));
			rt = value.toISOString();
			rt = rt.substring(0, rt.length - 1);
			rt = rt.split('T').join(' ');
			return rt;
		case SQLType.date:
			offset = value.getTimezoneOffset();
			value = new Date(value.getTime() - (offset * 60 * 1000));
			rt = value.toISOString().split("T")[0];
			return rt;
		case SQLType.time:
			return value;
		case SQLType.blob:
			return value.toString();
		default:
			return value;
	}
}