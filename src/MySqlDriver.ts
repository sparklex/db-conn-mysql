import { Driver, Connection, DataSource, Dialect } from "db-conn";
import { MySqlConnection } from "./MySqlConnection.js";
import { MySqlConnectionConfig } from "./MySqlConnectionConfig.js";

import * as mysql2 from 'mysql2/promise.js';
import { MySqlDataSource } from "./MySqlDataSource.js";
import * as URL from 'url';
import { MySqlDialect } from "./dialect/MySqlDialect.js";


export class MySqlDriver implements Driver {
	oDialect: Dialect = new MySqlDialect()
	public getDialect(): Dialect {
		return this.oDialect;
	}
	public getPool(url: string, props?: any): DataSource {
		const config = this.getConfig(url, props);
		const ds = new MySqlDataSource(config);
		return ds;
	}
	public acceptsURL(url: string): boolean {
		const rt = url.startsWith('mysql://');
		return rt;
	}

	public async connect(url: string, props?: any): Promise<Connection> {
		const config = this.getConfig(url, props);
		const _conn = await mysql2.createConnection(config);
		const rt = new MySqlConnection(_conn);
		return rt;
	}

	private getConfig(url: string, props?: any): MySqlConnectionConfig {
		const config: MySqlConnectionConfig = (props == null) ? {} : props;
		const u = URL.parse(url, false);
		const [user, password] = u.auth.split(":");

		if (user) {
			config.user = user;
		}
		if (password) {
			config.password = password;
		}
		config.host = u.hostname;
		if (u.port) {
			//config.host = config.host + ":" + u.port;
			config.port = parseInt(u.port);
		}
		const database = u.path.substring(1);

		if (database) {
			config.database = database;
		}
		config.supportBigNumbers = true;
		return config;
	}
}