import { DriverManager } from 'db-conn';
import '../index.js'

test("driver", async () => {
    expect(DriverManager.getDrivers().length).toStrictEqual(1);
});