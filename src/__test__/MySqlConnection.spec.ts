import { Connection, Result } from "db-conn";
import { MySqlDriver } from "../MySqlDriver.js";

const driver = new MySqlDriver();

let conn: Connection;
beforeEach(async() =>  {
	conn = await driver.connect("mysql://root:1234@localhost/test");
});
afterEach(async() => {
	await conn.close();
});
test('select', async () => {
	conn.setAutoCommit(true);
	let data = await conn.executeQuery("select 1 from dual");
	expect(data).toStrictEqual([{1:'1'}]);
});
test('wrong select', async () => {
	let error = false;
	try {
		await conn.executeQuery("drop table if exists test");
	} catch (e) {
		error = true;
	}
	expect(error).toStrictEqual(true);

});
test('insert', async () => {
	await conn.setAutoCommit(true);
	await conn.execute("drop table if exists test");
	await conn.execute("create table test (id int, data varchar(20), primary key(id))");
	let data = await conn.execute("insert into test(id,data) values(?,?)",[1,'a']);
	expect(data.affectedRows).toStrictEqual(1);
	await conn.close();
	conn = await driver.connect("mysql://root:1234@localhost/test");
	let data2 = await conn.executeQuery("select * from test");
	expect(data2).toStrictEqual([ {id:1,data:'a'} ]);
});
test('commit', async () => {
	await conn.setAutoCommit(false);
	await conn.execute("drop table if exists test");
	await conn.execute("create table test (id int, data varchar(20), primary key(id))");
	let data = await conn.execute("insert into test(id,data) values(?,?)",[1,'a']);
	expect(data.affectedRows).toStrictEqual(1);
	await conn.commit();
	let data2 = await conn.executeQuery("select * from test");
	expect(data2).toStrictEqual([ {id:1,data:'a'} ]);
});
test('rollback', async () => {
	await conn.setAutoCommit(false);
	await conn.execute("drop table if exists test");
	await conn.execute("create table test (id int, data varchar(20), primary key(id))");
	let data = await conn.execute("insert into test(id,data) values(?,?)",[1,'a']);
	expect(data.affectedRows).toStrictEqual(1);
	await conn.rollback();
	let data2 = await conn.executeQuery("select * from test");
	expect(data2).toStrictEqual([]);
});


test('sql types test', async () => {
	await conn.setAutoCommit(true);
	await conn.execute("drop table if exists type_test");
	await conn.execute("create table type_test (a int, b varchar(20),c decimal(19,6), d real, e timestamp(3), f date, g time, z json, primary key(a))");
	let data:Result = await conn.execute("insert into type_test(a,b,c,d,e,f,g,z) values(?,?,?,?,?,?,?,?)",
	[1,'中文', '1234567890123.456789',  1.7976931348623157E+308,
	'1999-12-31 23:59:59.123', '2000-01-31', '23:59:59', {a:99,b:78}]);

	expect(data.affectedRows).toStrictEqual(1);
	await conn.close();
	conn = await driver.connect("mysql://root:1234@localhost/test");
	data = await conn.execute("select * from type_test");
	expect(data.data?.length).toStrictEqual(1);
	const row = data.data?.[0] as any;
	expect(row.a).toStrictEqual(1);
	expect(row.b).toStrictEqual('中文');
	expect(row.c).toStrictEqual('1234567890123.456789');
	expect(row.d).toStrictEqual(1.7976931348623157E+308);
	expect(row.e).toStrictEqual('1999-12-31 23:59:59.123');
	expect(row.f).toStrictEqual('2000-01-31');
	expect(row.g).toStrictEqual('23:59:59');
	expect(row.z).toStrictEqual({a:99,b:78});

});


test('sql types test stream', async () => {
	await conn.setAutoCommit(true);
	await conn.execute("drop table if exists type_test");
	await conn.execute("create table type_test (a int, b varchar(20),c decimal(19,6), d real, e timestamp(3), f date, g time, z json, primary key(a))");
	let data:Result = await conn.execute("insert into type_test(a,b,c,d,e,f,g,z) values(?,?,?,?,?,?,?,?)",
	[1,'中文', '1234567890123.456789',  1.797693134862315E-300,
	'1999-12-31 23:59:59.123', '2000-01-31', '23:59:59', {a:99,b:78}]);

	expect(data.affectedRows).toStrictEqual(1);
	await conn.close();
	conn = await driver.connect("mysql://root:1234@localhost/test");
	const rs = await conn.executeQueryStream("select * from type_test");
	let exists = await rs.next()
	expect(exists).toStrictEqual(true);
	const row = rs.getRow();
	expect(row.a).toStrictEqual(1);
	expect(row.b).toStrictEqual('中文');
	expect(row.c).toStrictEqual('1234567890123.456789');
	expect(row.d).toBeCloseTo(1.797693134862315E-300);
	expect(row.e).toStrictEqual('1999-12-31 23:59:59.123');
	expect(row.f).toStrictEqual('2000-01-31');
	expect(row.g).toStrictEqual('23:59:59');
	expect(row.z).toStrictEqual({a:99,b:78});
	exists = await rs.next()
	expect(exists).toStrictEqual(false);

	await rs.close();
	data = await conn.execute("insert into type_test(a,b,c,d,e,f,g,z) values(?,?,?,?,?,?,?,?)",
	[2,'中文', '1234567890123.456789',  1.7976931348623157E+308,
	'1999-12-31 23:59:59.123', '2000-01-31', '23:59:59', {a:99,b:78}]);

	expect(data.affectedRows).toStrictEqual(1);	
});