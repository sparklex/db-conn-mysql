import { DriverManager } from "db-conn";
import '../index.js';

test('DriverManager.getConnection', async () => {
    const conn = await DriverManager.getConnection('mysql://root:1234@localhost:3306/test')
    await conn.close();
});
test('DriverManager.getPool', async () => {
    const pool = await DriverManager.getPool('mysql://root:1234@localhost:3306/test')
    const conn = await pool.getConnection()
    await conn.close();
    await pool.close();
});

test('DriverManager.getPoo 2l', async () => {
    const pool = await DriverManager.getPool('mysql://root:1234@localhost:3306/test', { connectionLimit: 1 })
    const conn = await pool.getConnection()
    await conn.close();
    await pool.close();
});