import { DriverManager } from "db-conn";
import '../index.js';


test('DriverManager.getPoo 2l', async () => {
    const pool = await DriverManager.getPool('mysql://root:1234@localhost:3306/test', { connectionLimit: 1 })
    const conn = await pool.getConnection();
    const rs = await conn.executeQueryStream("select 1 from dual");
	while(await rs.next()) {
		const data = await rs.getRow();
		console.debug(data);
	}
    await conn.close();
    await pool.close();
});