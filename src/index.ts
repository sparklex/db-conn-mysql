import { DriverManager } from "db-conn";
import { MySqlDriver } from "./MySqlDriver.js";

export * from "./MySqlDriver.js";
export * from "./MySqlConnection.js";
export * from "./MySqlConnectionConfig.js";
export * from "./MySqlDataSource.js"
export * from "./MySqlDataSourceConfig.js"


DriverManager.registerDriver(new MySqlDriver());